
import { HttpHeaders } from '@angular/common/http';

export const environment = {
  production: false,
  apiUrl: 'http://accutime-restful.us-east-2.elasticbeanstalk.com',
  httpOptions : {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'true',
          'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS'
        })
  }
};

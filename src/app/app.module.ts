import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UploadInvComponent } from './upload-inv/upload-inv.component';
import { ProdviewListComponent } from './prodview-list/prodview-list.component';
import { ClientviewComponent } from './clientview/clientview.component';
import { LightboxModule } from 'ngx-lightbox';
import { ItemCardComponent } from './item-card/item-card.component';
import { ProdviewCreateComponent } from './prodview-create/prodview-create.component';
import { JwtReqInterceptor } from './core/req.intercepter.service';
import { ErrorInterceptor } from './core/error.interceptor';
import { NotLoadedComponent } from './not-loaded/not-loaded.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'
import { CachingInterceptorService } from './core/caching-interceptor.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { DownloadComponent } from './download/download.component';

@NgModule({
  declarations: [
    AppComponent,
    UserProfileComponent,
    HomeComponent,
    LoginComponent,
    UploadInvComponent,
    ProdviewListComponent,
    ClientviewComponent,
    ItemCardComponent,
    ProdviewCreateComponent,
    NotLoadedComponent,
    DownloadComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    AppRoutingModule,
    ReactiveFormsModule,
    LightboxModule,
    NgMultiSelectDropDownModule,
    NgSelectModule,
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtReqInterceptor, multi: true },
              { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
            //  { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptorService, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

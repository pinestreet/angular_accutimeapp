export interface PVListItem{
  id:number,
  shortName:string,
  cob: string,
  url:string
}

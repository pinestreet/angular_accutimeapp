import { PvItem, PvKey } from "./pvItem";
import { FormGroup } from "@angular/forms";

export class WmsInvItem {
  itemNo: string;
  colorCode: string;
  refCode: string;
  qty: number;
  pono: string;
  imgUrl: string;
  imgData: any; // eventually we load it async

  pvItem: PvItem;
  categories: string[];
  brands: string[];
  types: string[];
  customers:string[];
}

export function wmsItemToPvItemConverter(wmsItem: WmsInvItem): PvItem {
  if (wmsItem.pvItem != null) {
    return wmsItem.pvItem;
  }
  const pvKey = new PvKey();
  pvKey.itemNo = wmsItem.itemNo;

  wmsItem.pvItem = new PvItem();
  wmsItem.pvItem.id = pvKey;
  wmsItem.pvItem.colorCode = wmsItem.colorCode;
  wmsItem.pvItem.qty = wmsItem.qty;
  wmsItem.pvItem.imgData = wmsItem.imgData;
  wmsItem.pvItem.refCode = wmsItem.refCode;
  wmsItem.pvItem.pono = wmsItem.pono;

  return wmsItem.pvItem;
}

export function formItemToWmsItem(createPvForm: FormGroup): any {
  //return a new object with form value
  if (createPvForm.value == null) {
    return {};
  }

  return {
    pvShortName: createPvForm.value.pvShortName,
    pvFullName: createPvForm.value.pvFullName,
    itemsList: createPvForm.value.itemsList,
    itemPrefix: createPvForm.value.itemPrefix,
    itemNoFrom: createPvForm.value.itemNoFrom,
    itemNoTo: createPvForm.value.itemNoTo,
    itemIncludeAll: createPvForm.value.itemIncludeAll,
    ponoFrom: createPvForm.value.ponoFrom,
    ponoTo: createPvForm.value.ponoTo,
    customers: createPvForm.value.customers.length === 0 ? [] : createPvForm.value.customers,
    categories: createPvForm.value.categories.length === 0 ? [] : createPvForm.value.categories,
    brands: createPvForm.value.brands.length === 0 ? [] : createPvForm.value.brands,
    types: createPvForm.value.types.length === 0 ? [] : createPvForm.value.types,
  };
}

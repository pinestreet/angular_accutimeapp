
export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  token?: string;

  address1: string;
  city: string;
  State: string;
  zip: string;

  skills: string;
  phone: string;
  birthDate: Date;
  gender: string;
}


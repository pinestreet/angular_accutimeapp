import { WmsInvItem } from './WmsInvItem';

export class PvItem {
  id: PvKey;
  refCode: string;
  qty: number;
  colorCode: string;
  imgUrl: string;
  imgData: any; // eventually we load it async
  isIncluded: boolean = true; // include/exclude item when creating pv
  pono: string;
}

export class PvKey {
  pvId: number;
  itemNo: string;
  //colorCode: string;  // have to have colorCode here too
}


export function pvItemsToWMSItemConverter(pvItem: PvItem): WmsInvItem {

  //ToDO:
    return new WmsInvItem;
}

import { PvItem } from './pvItem';

export class ProductView{
 shortName: string;
 fullName: String;
// cob: String;
 pvItems: PvItem[];

  constructor(shortName: string, fullName: String, pvItems: PvItem[]){
    this.shortName = shortName;
    this.fullName = fullName;
    this.pvItems =pvItems;
  }
}



import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  private errorMsg: string;
  private response: any;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', responseType: 'text' })
  };
  error: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private http: HttpClient,
  ) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      userName: new FormControl(),
      password: new FormControl(),
    });
  }

 get formValues(){ return this.loginForm.controls;}

  onSubmit() {
    var postData = JSON.stringify(this.loginForm.value);
    this.authService.login(postData)
    //this.authService.login_dummy(postData)
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from route parameters or default to '/'
          const returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
          this.router.navigate([returnUrl]);
        },
        error: (error: string) => {
          console.log("login Error:" + error);
          this.error = error;
          this.authService.logout();
          this.router.navigate(['login']);
        }
      });
  }
}

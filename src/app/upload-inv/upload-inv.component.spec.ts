import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadInvComponent } from './upload-inv.component';

describe('UploadInvComponent', () => {
  let component: UploadInvComponent;
  let fixture: ComponentFixture<UploadInvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadInvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadInvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

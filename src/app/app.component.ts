import { Component, OnInit, Input } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { AuthService } from './core/auth.service';
import { User } from './models/user';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

   isLoggedIn = false;
   sidenav = false;
   currentUser?: User;
   private userLoginSubscription: Subscription;

   isMenuOpen = true;
   contentMargin = 240;

   constructor(private router: Router, private authSerice: AuthService) {
      this.userLoginSubscription = this.authSerice.currentUser
      .subscribe( usr => { this.currentUser = usr;
                           this.isLoggedIn = localStorage.getItem('isLoggedIn') === 'true'; }
              ) ;
  }

  ngOnInit(): void {
    this.isLoggedIn = 'true' === localStorage.getItem('isLoggedIn');
  }

  onToolbarMenuToggle() {
    if (!this.isLoggedIn)
      return;

    console.log('On toolbar toggled', this.isMenuOpen);
    this.isMenuOpen = !this.isMenuOpen;

    if(!this.isMenuOpen) {
      this.contentMargin = 70;
    } else {
      this.contentMargin = 240;
    }
  }


  onLogout() {
    console.log(' Logout called from appComp ');
    this.authSerice.logout();
    this.router.navigate(['login']);
    this.isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';
  }
}

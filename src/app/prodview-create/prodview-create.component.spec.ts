import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdviewCreateComponent } from './prodview-create.component';

describe('ProdviewCreateComponent', () => {
  let component: ProdviewCreateComponent;
  let fixture: ComponentFixture<ProdviewCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdviewCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdviewCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

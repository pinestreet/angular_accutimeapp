import { Component, OnInit } from "@angular/core";
import { Form, FormBuilder, Validators } from "@angular/forms";
import { PvItem } from "../models/pvItem";
import { Observable, of, pipe, Subscriber, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { WmsInvItem, wmsItemToPvItemConverter, formItemToWmsItem, } from "../models/WmsInvItem";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { ProductView } from "../models/ProductView";
import { CloseScrollStrategy } from "@angular/cdk/overlay";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { NgSelectModule } from '@ng-select/ng-select';
import { environment } from 'src/environments/environment';
//import * as FileSaver from 'file-saver';


@Component({
  selector: "app-prodview-create",
  templateUrl: "./prodview-create.component.html",
  styleUrls: ["./prodview-create.component.css"],
})
export class ProdviewCreateComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };
  httpImgOptions = {
    headers: new HttpHeaders({ "Content-Type": "image/jpeg" }),
  };

  httpOptionsExcel:Object = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
      responseType : "blob",

  };


  categorySource = [ ];
  brandSource = [ ];
  typeSource = [ ];
  customerSource = [ ];

  customerDropdownSource = [];
  selectedCustomerDropdown = [];
  customerDropdownSettings = {
    singleSelection: false,
    idField: "item_id",
    textField: "item_text",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };

  subscriptionList: Array<Subscription> = [];

  // createPvForm: Form;
  pvItems: Array<PvItem> = [];
  pvSearchItemsObservable: Observable<WmsInvItem[]>;
  pvCreatePvObservable: Observable<string>; // pvId can be returned
  pvCreatePvExcelObservable: Observable<any>; // excel can be returned

  createPvForm = this.formBuilder.group({
    pvShortName: [""], // map to FormControlName
    pvFullName: [""],
    itemsList: [""],
    itemPrefix: [""],
    itemNoFrom: [""],
    itemNoTo: [""],
    itemIncludeAll: [""],
    ponoFrom: [""],
    ponoTo: [""],
    customers: [""],
    categories: [""],
    brands: [""],
    types: [""],
  });

  /* inject formbuilder */
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    //var customersObservable: Observable<string[]> =
    this.populateCustomers();
    this.populateCategoris();
    this.populateBrands();
    this.populateTypes();
    this.populateCustomerSource();
  }

  private populateCustomers() {
    this.http.get<string[]>(`${environment.apiUrl}/get_customers`, this.httpOptions).subscribe(
      (response) => {
        //console.log(" Called for customer Data: " + response);
        var cnt: number = 0;

        // Assign the data to the data source for the table to render
        this.customerDropdownSource= (<String[]>response).map((e) => {
          return { item_id: cnt++, item_text: e };
        });
      },
      (error: any) => {
        console.error(
          "Cust-list#1 error: " + error.err + "err.status" + error.statusText
        ); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          //tost
        }
      }
    ),
      (error: string) => console.error("error: " + error);
  }

  private populateCustomerSource() {
    this.http.get<string[]>(`${environment.apiUrl}/get_customers`, this.httpOptions).subscribe(
      (response) => {
        this.customerSource= (<String[]>response);
      },
      (error: any) => {
        console.error(
          "Customer list error: " + error.err + "err.status" + error.statusText
        ); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          //tost
        }
      }
    ),
      (error: string) => console.error("error: " + error);
  }

  private populateCategoris() {
    this.http.get<string[]>(`${environment.apiUrl}/get_categories`, this.httpOptions).subscribe(
      (response) => {
        this.categorySource= (<String[]>response);
      },
      (error: any) => {
        console.error(
          "Category list error: " + error.err + "err.status" + error.statusText
        ); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          //tost
        }
      }
    ),
      (error: string) => console.error("error: " + error);
  }


  private populateBrands() {
    this.http.get<string[]>(`${environment.apiUrl}/get_brands`, this.httpOptions).subscribe(
      (response) => {
        this.brandSource= (<String[]>response);
      },
      (error: any) => {
        console.error(
          "Category list error: " + error.err + "err.status" + error.statusText
        ); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          //tost
        }
      }
    ),
      (error: string) => console.error("error: " + error);
  }

  private populateTypes() {
    this.http.get<string[]>(`${environment.apiUrl}/get_types`, this.httpOptions).subscribe(
      (response) => {
            this.typeSource= (<String[]>response);
      },
      (error: any) => {
        console.error(
          "Category list error: " + error.err + "err.status" + error.statusText
        ); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          //tost
        }
      }
    ),
      (error: string) => console.error("error: " + error);
  }

  searchItems() {
    console.log(" Search Terms: " + JSON.stringify(this.createPvForm.value));
    //console.log("types: " + this.createPvForm.value.types );
    let formData = formItemToWmsItem(this.createPvForm);
    if (Object.keys(formData).length == 0) {
      this.openSnackBar(
        " Items searched for " + this.createPvForm.value.pvShortName,
        "Search"
      );
    }

    this.pvSearchItemsObservable = this.http.post<WmsInvItem[]>(`${environment.apiUrl}/prod_views_search/`,
      JSON.stringify(formData),
      this.httpOptions
    );

    var sub1: Subscription = this.pvSearchItemsObservable.subscribe(
      (response) => {
        if (Object.keys(response).length == 0) {
          this.snackBar.open(
            " Search did not find any results " + JSON.stringify(formData),
            "Search",
            { duration: 6000 }
          );
        }
        this.pvItems = response.map((i) => wmsItemToPvItemConverter(i));
        this.pvItems.forEach((pvItem) => {
          this.subscriptionList.push(
            this.http
              .get(`${environment.apiUrl}/pv_item_img/` + pvItem.id.itemNo, this.httpImgOptions)
              .subscribe((imgItemRes) => {
                pvItem.imgData = imgItemRes["image"];
              })
          );
        });
      },
      (error) => console.error("error: " + error.err + ", err.status: " + error.statusText)
    );

    this.subscriptionList.push(sub1);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptionList.forEach((s) => s.unsubscribe());
  }

  /*
    Send restful post request to create productview
  */
  createPv() {
    console.log(" Creating Pv.: " + JSON.stringify(this.createPvForm.value));

    this.pvItems.map((x) => (x.imgData = null)); // remove image data reduce load. should we create/copy new PvItems?
    var selectedPvItems:PvItem[] = this.pvItems
                .filter(pvItem => pvItem.isIncluded == true);


    var requestedPv = new ProductView(
      this.createPvForm.value.pvShortName,
      this.createPvForm.value.pvFullName,
      selectedPvItems
    );
    console.log("PV:" + requestedPv.pvItems);
    this.pvCreatePvObservable = this.http.post<string>(`${environment.apiUrl}/prod_view_create/`,
      JSON.stringify(requestedPv),
      this.httpOptions
    );
    this.pvCreatePvObservable.subscribe(
      (response) => {
        var newPvURL: string = `${environment.apiUrl}/prodview/` + response;
        this.router.navigate([newPvURL]);
      },
      (error) => console.error("error: " + error)
    );
  }

  /*
   sends create excel of selected items
*/
  createPvExcel() {
    console.log(" Creating Excel2: " + JSON.stringify(this.createPvForm.value));

    let selectedPvItemse = this.pvItems.map((x) => Object.assign({}, x));
//todo: can we do it locally using xlsx pkg?
//    var tmepup = selectedPvItemse.map((x) => (x.imgData = null));

//    selectedPvItemse.forEach((pvItem) => console.log(pvItem));

    var requestedPv = new ProductView(
      this.createPvForm.value.pvShortName,
      this.createPvForm.value.pvFullName,
      selectedPvItemse
    );

    this.http.post<any>( `${environment.apiUrl}/prod_view_excel_create/`, JSON.stringify(requestedPv), this.httpOptionsExcel)
    .subscribe(
      (response) => {
        console.log(" Received response for Excel query");
        this.downloadFile(response)

      },
      (error) => console.error("error dfsfd: " , error)
    );

  }


  downloadFile(data: any) {
    const blob = new Blob([data], { type: 'application/vnd.ms-excel' });
    const url= window.URL.createObjectURL(blob,);
    var a = document.createElement("a");
    a.href = url;
    a.download = this.createPvForm.value.pvShortName;
    a.click();
    window.URL.revokeObjectURL(url);

  }

  /* Checks if all combination of the form inputs are correct. */
  errorsOnForm() {
    const formErros =
      this.createPvForm.value.pvShortName &&
      this.createPvForm.value.pvFullName &&
      (this.createPvForm.value.itemsList ||
        (this.createPvForm.value.itemPrefix &&
          (this.createPvForm.value.itemNoFrom ||
            this.createPvForm.value.itemNoTo)) ||
        this.createPvForm.value.ponoFrom ||
        this.createPvForm.value.ponoTo ||
        this.createPvForm.value.customers ||
        this.createPvForm.value.categories ||
        this.createPvForm.value.brands ||
        this.createPvForm.value.types);
    return !formErros;
    // return !this.createPvForm.valid;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`log: ${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
}

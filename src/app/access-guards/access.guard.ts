
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../core/auth.service';
import { TimeoutError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {

  constructor(private authService: AuthService, public router: Router) { }


  canActivate(activeRoute: ActivatedRouteSnapshot, rStateSanpshot: RouterStateSnapshot) {
    // console.log(' AccessGuard called from - stateSanp :' + rStateSanpshot.toString() + ', Active route: ' + activeRoute.url);
    const clientViewMatcher = 'prodview/'; // '\/prodview\/\d+$';  // /prodview/529

    if (rStateSanpshot.url.match(clientViewMatcher) != null) {
      return true; // client view pass
    }

    if (localStorage.getItem('isLoggedIn') !== 'true') {
      this.router.navigate(['login']);
      return false;
    }

    // this.authService.canAccess("ACTIONS.ACCESS_PROFILE");
    return true;
  }


}

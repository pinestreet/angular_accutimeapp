
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../core/auth.service';


@Injectable({
  providedIn: 'root'
})
export class PassAllAccessGuard implements CanActivate {

  constructor(private authService: AuthService, public router: Router) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // this.authService.canAccess("ACTIONS.ACCESS_PROFILE");
    return true;
  }


}

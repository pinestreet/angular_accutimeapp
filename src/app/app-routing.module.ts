import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UploadInvComponent } from './upload-inv/upload-inv.component';
import { ProdviewListComponent } from './prodview-list/prodview-list.component';
import { ClientviewComponent } from './clientview/clientview.component';
import { AccessGuard } from './access-guards/access.guard';
import { PassAllAccessGuard } from './access-guards/passAll.guard';
import { ProdviewCreateComponent } from './prodview-create/prodview-create.component';
import { NotLoadedComponent } from './not-loaded/not-loaded.component';
import {DownloadComponent} from "./download/download.component";

/*
const routes: Routes = [
  { path: '', redirectTo: '/home' , pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: UserProfileComponent, canActivate: [AccessGuard] }
];
*/

// https://stackblitz.com/edit/angular-yjkbnf?file=app%2Fapp.module.ts
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '',   component: HomeComponent,
    children: [
      { path: '',               component: ProdviewListComponent,     canActivate: [AccessGuard] },
      { path: 'home',           component: HomeComponent,             canActivate: [AccessGuard] },
      { path: 'profile',        component: UserProfileComponent,      canActivate: [AccessGuard] },
      { path: 'prodviewlist',   component: ProdviewListComponent,     canActivate: [AccessGuard] },
      { path: 'createPv',       component: ProdviewCreateComponent,   canActivate: [AccessGuard] },
      { path: 'upload',         component: UploadInvComponent,        canActivate: [AccessGuard] },
      { path: 'prodview/:id',   component: ClientviewComponent                                   },  // open url
      //{ path: '**',             component: FourZeroFourComponent                                 },  // open url
      { path: 'notLoaded',      component: NotLoadedComponent,      canActivate: [AccessGuard] },
      { path: 'userProflie',    component: UserProfileComponent,    canActivate: [AccessGuard] },
      { path: 'download',    component: DownloadComponent    }, //open url
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, { useHash: true })], /* hash strategy for routing */
  exports: [RouterModule]
})
export class AppRoutingModule { }

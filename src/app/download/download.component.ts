import {Component, OnInit} from '@angular/core';


export interface DownloadData {
  name: string;
  instructions: string;
  software_link: string;
  href_instr: string;
  href_software: string;
}

const ELEMENT_DATA: DownloadData[] = [
  {
    name: "T5 Watch AP",
    instructions: 'T5 Instructions',
    software_link: 'Download',
    href_instr: './asset/docs/T5_Watch_Ap_Instructions.jpg',
    href_software: './asset/docs/Watch_T5_Software.zip'
  },
  {
    name: "A6-A7",
    instructions: 'A6-A7 Instructions',
    software_link: 'Download',
    href_instr: './asset/docs/A6-Instructions.jpg',
    href_software: './asset/docs/Watch_A6_Software.zip'
  },
];


@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  displayedColumns: string[] = ['Model', 'Instructions', 'Software Link'];
  dataSource = ELEMENT_DATA;

  ngOnInit(): void {
  }

}

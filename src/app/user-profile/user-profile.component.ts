import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  userId: string;
  response: any;

  constructor(private http: HttpClient) {

  }

  ngOnInit() {

  }


    getInfo() {
//      console.log(' User is: ' + this.userId);
      /*
        https://stackoverflow.com/questions/43871637/no-access-control-allow-origin-header-is-present-on-the-requested-resource-whe
        do proxy config
        */

      this.http.get(`${environment.apiUrl}/user/` + this.userId, {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      })
        .subscribe((response) => {
          this.response = response;
          console.log(this.response);
        });
    }

}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Operator, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject?: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  httpOptions = environment.httpOptions
  httpOptions2 = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': 'true',
          'Access-Control-Allow-Methods':  'GET,POST,PUT,DELETE,OPTIONS'
        })
  };

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser') || '{}' ));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }

  login(postData: String) {
    // return this.http.post<any>(`${environment.apiUrl}/user_login`, { username, password }, this.httpOptions)
    return this.http.post<any>(`${environment.apiUrl}/user_login`, postData, this.httpOptions)
    .pipe( map(
        user => {
         // console.log(' Req : ' + postData);
            // remove any existing tokens
          localStorage.removeItem('currentUser');
          localStorage.removeItem('isLoggedIn');
          localStorage.removeItem('token');
            // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('isLoggedIn', 'true');
          localStorage.setItem('token', user['token']);
          this.currentUserSubject.next(user);
          return user;
        }));
  }

  login_dummy(postData: String) {
    let user:User = {
        id:1,
        username: 'localUser',
        password: 'localPass',
        firstName:'',
        lastName:'',
        address1:'',
        city:'',
        State:'',
        zip:'',
        skills:'',
        phone:'',
        birthDate: new Date(),
        gender:'',
        token:'myLocalTempTockenSecrate'
    }

    localStorage.removeItem('currentUser');
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');
      // store user details and jwt token in local storage to keep user logged in between page refreshes
    localStorage.setItem('currentUser', JSON.stringify(user));
    localStorage.setItem('isLoggedIn', 'true');
    localStorage.setItem('token', user['token']);
    this.currentUserSubject.next(user);
    return of(user);

  }

}

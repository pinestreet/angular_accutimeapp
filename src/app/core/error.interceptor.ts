import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthService } from "./auth.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthService,
    private snackBar: MatSnackBar
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        let errMsg = err.message ? err.message : err.toString();
        console.log("err: " + errMsg);

        let loginRetryMsg = "Login Failed. Retry in a few minutes";
        let appErrorMsg =
          "Accutime App (Error Interceptor): Error occured, Pls. logout and login again - Contact Support : \n" +
          err.statusText;
        let appMsg = errMsg.includes("login") ? loginRetryMsg : appErrorMsg;

        let snackBarRef = this.snackBar.open(appMsg, "Dismiss", { duration: 50000,});
        snackBarRef.onAction().subscribe(() => {snackBarRef.dismiss();});

        if (err.status === 401 || err.status === 403) {
          // auto logout if 401 response returned from api
          this.authenticationService.logout();
          location.reload(true);
        }

        const error = err.error || err.statusText;
        return throwError(err);
      })
    );
  }
}

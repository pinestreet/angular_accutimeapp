import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpResponse,
} from "@angular/common/http";
import { Observable, throwError, of } from "rxjs";
import { share, catchError, map, tap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class CachingInterceptorService implements HttpInterceptor {
  private cache: Map<string, HttpResponse<any>> = new Map();

  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.method !== "GET") {
      return next.handle(request);
    }

    //check if we have response available
    const cachedResponse: HttpResponse<any> = this.cache.get(request.url);

    if (cachedResponse) {
      console.log(`FOUND IN Cache: ${request.url}`);
      return of(cachedResponse.clone());
    } else {
      console.log(`Not FOUND IN Cache: ${request.url}`);
      return next.handle(request).pipe(
        catchError((err) => {
          //invalidate cache entry
          this.cache.delete(request.url);
          return throwError(err); //continue with error.. or show tost ? log out? it should be handlled by error service
        }),
        tap((event) => {
          if (event instanceof HttpResponse) {
            console.log(`Adding item to cache: ${request.url}`);
            this.cache.set(request.url, event);
          } else {
            console.log(` cache interceptor - Event is not httpResponse url: ${request.url}, response: ${event}`);
          }
        })
      );
    }
  }

  removeCacheElement(httpRequest: HttpRequest<any>) {
    this.cache.delete(httpRequest.url);
  }
}

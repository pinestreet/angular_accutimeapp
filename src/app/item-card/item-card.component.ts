import { Component, OnInit, Input } from '@angular/core';
import { PvItem } from '../models/pvItem';
import { Lightbox, IAlbum } from 'ngx-lightbox';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})


export class ItemCardComponent implements OnInit {

  @Input()
  pvItem: PvItem;

  @Input()
  isReadOnly: boolean;

  constructor(private lightbox: Lightbox, private snackBar: MatSnackBar) { }

  ngOnInit() {
    console.log('pvItem: ' + this.pvItem.id.itemNo);
  }

  // opening light box
  openLightBox( pvItem) {
    const onePickAlbums: Array<IAlbum> = [];

    const album = {
      src: 'data:image/JPEG;base64,' + pvItem.imgData,
      caption: 'Item: ' + pvItem.id.itemNo + ', Ref: ' + pvItem.refCode,
      thumb: 'thumb'
    };
    onePickAlbums.push(album);
    // open lightbox
    this.lightbox.open(onePickAlbums, 0);

  }

  // opening light box
  disableCard( pvItem: PvItem) {
    const onePickAlbums: Array<IAlbum> = [];
    pvItem.isIncluded = !pvItem.isIncluded;
    console.log(pvItem.isIncluded);
    this.snackBar.open(pvItem.id.itemNo , 'Item ' + (pvItem.isIncluded ? 'Included': 'Excluded'), {duration: 2000,});

  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PvItem } from '../models/pvItem';
import { Observable } from 'rxjs';
import { map, mergeMap, concatAll } from 'rxjs/operators';
import { Lightbox, IAlbum } from 'ngx-lightbox';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-clientview',
  templateUrl: './clientview.component.html',
  styleUrls: ['./clientview.component.css']
})

export class ClientviewComponent implements OnInit, OnDestroy {

  pvid: string;
  pvItems: Array<PvItem> = [];
  pvListItemsObservable: Observable<PvItem[]>;
  response: any;

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private lightbox: Lightbox) {

  }
  ngOnDestroy(): void {
    // this.pvListItemsSubscription.unsubscribe();
  }

  ngOnInit() {
    this.pvid = this.activatedRoute.snapshot.params.id;
    console.log(' Client component called for id: ' + '/prod_view_items/' + this.pvid);

    this.pvListItemsObservable = this.http.get<PvItem[]>(`${environment.apiUrl}/prod_view_items/` + this.pvid, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' , })
    });

    this.pvListItemsObservable.subscribe((response) => {
      console.log(' Called for pv items Data');
      response.forEach(pvItem => {
        pvItem.isIncluded = true;
        this.http.get(`${environment.apiUrl}/pv_item_img/` + pvItem.id.itemNo, {
          headers: new HttpHeaders({ 'Content-Type': 'image/jpeg' })
        })
          .subscribe((imgItemRes) => {
            pvItem.imgData = imgItemRes['image'];
          });
      });
      this.pvItems = response;

    }, error => console.error('error: ' + error));
  }

  openLightBox(pvItem) {
    const onePickAlbums: Array<IAlbum> = [];

    const album = {
      src: 'data:image/JPEG;base64,' + pvItem.imgData,
      caption: 'Item: ' + pvItem.id.itemNo + ', Ref: ' + pvItem.refCode,
      thumb: 'thumb'
    };
    onePickAlbums.push(album);
    // open lightbox
    this.lightbox.open(onePickAlbums, 0);

  }
}




/*
    .map(image => image.text)
        .subscribe((imgItemRes) => {
          let imgData = 'data:image/png;base64,' + imgItemRes['image'];
          pvItem.imgData = sanitizer.bypassSecurityTrustUrl(imgData);
        });
      });


      // .pipe(
      //   map(itemNo => this.http.get('/prod_view_items/' + itemNo),
      //     concatAll()

      // .pipe(
      //   map(allPvItems => allPvItems.forEach(pvItem => {
      //     this.http.get<PvItem[]>('/prod_view_items/' + pvItem.id.itemNo, {
      //       headers: new HttpHeaders({ 'Content-Type': 'image/jpeg' })
      //     });
      //     console.log(' item id: ' + pvItem.id.itemNo);

      //   }))

      // );
*/

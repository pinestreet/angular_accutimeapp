import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
// import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PvItem } from '../models/pvItem';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '../core/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PVListItem } from '../models/pvListItem';
import { environment } from 'src/environments/environment';

export interface ProdViewData {
  viewId: number;
  viewName: string;
  cob: string;
  prductURL: string;
  action: string;
}

@Component({
  selector: 'app-prodview-list',
  templateUrl: './prodview-list.component.html',
  styleUrls: ['./prodview-list.component.css']
})


export class ProdviewListComponent implements OnInit, OnDestroy {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  routerL = '/provdview'; // ['/prodview', row.id]

  // displayedColumns: string[] = ['View id', 'View Name', 'Cob', 'Prodcut View URL', 'Action'];
  displayedColumns: string[] = ['id', 'shortName', 'cob', 'viewUrl', 'Action'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  response: any;
  pvListSubscription: Subscription;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private http: HttpClient, private router: Router, private authService: AuthService,   private snackBar: MatSnackBar,) {
  }

  ngOnDestroy(): void {
    this.pvListSubscription.unsubscribe();
  }

  ngOnInit() {
    this.pvListSubscription = this.http.get(`${environment.apiUrl}/prod_views`, this.httpOptions)
      .subscribe((response) => {
        this.response = response;
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator.pageSizeOptions = this.getPageSizeOptions();
        this.dataSource.sort = this.sort;

      },(error: any) =>{
        console.error('PV-list#1 error: '+ error.err +'err.status'+ error.statusText); // log to console instead
        if (error.status === 403) {
          // auto logout if 401 response returned from api
          this.authService.logout();
          this.router.navigate['login'];
        }
      }
    )
    , catchError(
        this.handleError<PvItem[]>('prod_views', [] )
      );

  }

  getPageSizeOptions() {
    return [25, 50, this.dataSource.data.length];
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  copyHrefLink(pvid): void {
    const uri = this.router.createUrlTree(['/prodview', pvid]).toString();
    const baseUrl = window.location.href.replace(this.router.url, '');
    const url = baseUrl + uri;

    const listener = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (url));
      e.preventDefault();
    };

    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);
  }

  deletePv(pvListItem: PVListItem): void {
    console.log('deleting pvid: '+ pvListItem.id);
    var x:Subscription = this.http.post(`${environment.apiUrl}/prod_delete/`+ pvListItem.id, this.httpOptions)
    .subscribe((response) => {
      console.log(pvListItem.id +': delete successful pvid: '+ pvListItem.shortName);
      this.dataSource.data = this.dataSource.data.filter(pvListItem =>  pvListItem.id != pvListItem.id);
      this.dataSource._updateChangeSubscription();
      //let snackBarRef =this.snackBar.open(pvListItem.id +': Product View Deleted: '+ pvListItem.shortName, "Dismiss", { duration: 5000,});
      //snackBarRef.onAction().subscribe(() => {snackBarRef.dismiss();});

    },(error: any) =>{
      console.error('PV-delete error: '+ error.err +'err.status'+ error.statusText);
      if (error.status === 403) {
        this.authService.logout();
        this.router.navigate['login'];
      }
    }
  );

  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('PV-list error: '+ error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`log: ${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdviewListComponent } from './prodview-list.component';

describe('ProdviewListComponent', () => {
  let component: ProdviewListComponent;
  let fixture: ComponentFixture<ProdviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdviewListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
